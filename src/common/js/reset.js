let ConfData =  {
  httpUrl:''
};
let objA={
  getUrlParam:function(name){
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //
    let r = window.location.search.substr(1).match(reg);  //
    if (r != null) return unescape(r[2]);
    return null; //
  },
  setCookie:function(name,value,time){
    let Days = time;
    let exp = new Date();
    exp.setTime(exp.getTime() + Days*24*60*60*1000);
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString()+";path=/";
  },
  getCookie:function(name){
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
    if(arr=document.cookie.match(reg))
      return unescape(arr[2]);
    else
      return null;
  }
}

export {ConfData,objA};
